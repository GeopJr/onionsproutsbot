#!/usr/bin/env python3
#
# This file is part of OnionSpoutsBot, a Tor Browser distribution Telegram bot.
#
# :authors: Panagiotis "Ivory" Vasilopoulos <git@n0toose.net>
#
# :copyright:   (c) 2020-2022, Panagiotis "Ivory" Vasilopoulos
#
# :license: This is Free Software. See LICENSE for license information.
#

from config import *
from files import *

import asyncio
import logging
import requests
import sqlite3
import urllib

from time import time
from requests import HTTPError
from pyrogram import Client, filters
from pyrogram.types import (InlineQueryResultArticle, InputTextMessageContent,
                            InlineKeyboardMarkup, InlineKeyboardButton,
                            KeyboardButton, ReplyKeyboardMarkup)

global response
global conn
global curs
response = requests.get(endpoint).json()

OnionSproutsBot = Client(
    "OnionSproutsBot",
    api_id,
    api_hash,
    bot_token=bot_token
)

# TO-DO: Async logging
logging.basicConfig(
    filename="onionsproutsbot.log",
    encoding='utf-8',
    level=logging.DEBUG
)

logging.getLogger().addHandler(logging.StreamHandler()) # prints to stderr

'''
TO-DO: Ask for the locale when the user initiates the bot, rather
than afterwards, and change the language the bot speaks automatically.

The language should be asked for initially before showing this selection screen.
That way, the instructions and all relevant documentation will be readable and completely
understood by the user. For that reason, the bot needs to cache a list of available (or all)
locales in the future.
'''

@OnionSproutsBot.on_message(filters.command("start"))
async def start_command(client, message):
        await client.send_message(
            chat_id=message.chat.id,
            text="Hi, welcome to OnionSproutsBot! What would you like to do?",
	        reply_markup=InlineKeyboardMarkup(
	            [
                    [InlineKeyboardButton("Download Tor Browser from Telegram", "request_tor")],
                    [InlineKeyboardButton("Official Tor Browser mirrors", "request_tor_mirrors")],
                    [InlineKeyboardButton("What is Tor?", "explain_tor")]
                ]
            )
        )

@OnionSproutsBot.on_callback_query(filters.regex("request_tor_mirrors"))
async def send_mirrors(client, callback):
    await client.send_message(
        chat_id=callback.from_user.id,
        text="- Internet Archive: https://archive.org/details/tor_project_archives\n"
        "- GitLab: https://gitlab.com/thetorproject/torbrowser-version-arch/raw/master/\n"
        "- GitHub: https://github.com/torproject/torbrowser-releases/releases/download/torbrowser-release/\n"
        "- Email: gettor@torproject.org (Include your operating system: Windows, macOS, Linux)\n\n"
        "(Make sure that your email provider is safe!)\n"
    )

    await client.answer_callback_query(
        callback.id
    )

@OnionSproutsBot.on_callback_query(filters.regex("explain_tor"))
async def send_explanation(client, callback):
    await client.send_message(
        chat_id=callback.from_user.id,
        text="Tor is a network that lets you anonymously connect to the Internet. "
        "It's a kind of peer-to-peer network, in which people can connect to each other, "
        "without any central server or middleman.\n"
        "The Tor network is a kind of onion routing network, and it's a kind of "
        "hidden service network.\n\n"
        "Tor is a software project that aims to make the Internet a safer, more private, "
        "and more secure place.\n\n"
        "In other words, Tor is a network that lets you connect to the Internet anonymously."
    )

    await client.answer_callback_query(
        callback.id
    )


'''
People generally don't cryptographically verify binaries. It could be a
good idea to take advantage of the nature of an IM platform and teach
people how to do that in a user-centered manner. Splitting this into
a selection prompt where the user consents to being sent a copy of Tor
could be beneficial.
'''

@OnionSproutsBot.on_callback_query(filters.regex("request_tor"))
async def tor_requested(client, callback):
    await client.send_message(
        chat_id=callback.from_user.id,
        text="Got it! I will have to ask you a couple of questions first.",
    )

    platform_keyboard = []

    for platform in response['downloads'].keys():
        platform_keyboard.append(InlineKeyboardButton(
            text=platform,
            callback_data='select_locale:' + platform
            )
        )

    platform_markup = InlineKeyboardMarkup([platform_keyboard])
    await client.send_message(callback.from_user.id, "Which operating system are you using?", reply_markup=platform_markup)

    await client.answer_callback_query(
        callback.id
    )


@OnionSproutsBot.on_callback_query(filters.regex("select_locale:"))
async def locale_selected(client, callback):
    counter = 1
    row = []
    locale_keyboard = []
    platform = callback.data.split(':')[1]

    for locale in response['downloads'][platform].keys():
        row.append(InlineKeyboardButton(
            text=locale,
            callback_data='download_tor:' + platform + ':' + locale
            )
        )

        if counter % 6 == 0:
            locale_keyboard.append(row)
            row = []

        counter += 1;

    locale_markup = InlineKeyboardMarkup(locale_keyboard)
    await client.send_message(
        chat_id=callback.from_user.id,
        text="What is your preferred language?",
        reply_markup=locale_markup
    )

    await client.answer_callback_query(
        callback.id
    )


@OnionSproutsBot.on_callback_query(filters.regex("download_tor"))
async def send_tor(client, callback):
    global conn, curs
    logging.debug(f"Command issued: {callback.data}")
    platform = callback.data.split(':')[1]
    locale = callback.data.split(':')[2]
    user_id = callback.from_user.id

    '''
    Detecting the language that the user speaks is not reliable
    and the codes that Telegram gives us appear as `en`, rather
    than e.g. `en-US`, which is what the endpoint needs. Manual
    conversions are unreliable and bothersome.

    The code used to test this was:
    `locale = callback.from_user.language_code`
    '''

    tor_sig_url = response['downloads'][platform][locale]['sig']
    tor_bin_url = response['downloads'][platform][locale]['binary']

    tor_sig_original_name = tor_sig_url.rsplit('/')[-1]
    tor_bin_original_name = tor_bin_url.rsplit('/', 1)[-1]

    results = curs.execute(
        f'''SELECT binary, binary_id, sig, sig_id FROM tor_releases WHERE (
             binary = "{tor_bin_original_name}"
        );'''
    ).fetchone()

    if results != None:
        print(results[1])
        # Sends the cached version of the selected binary
        await client.send_cached_media(user_id, file_id=results[1])
        # Sends the cached version of the selected signature
        await client.send_cached_media(user_id, file_id=results[3])
    else:
        await client.send_message(
            user_id,
            f"The version you have selected **({platform}, {locale})** "
             "hasn't been uploaded to Telegram's servers yet. Please wait."
        )
        await client.send_message(user_id, "Uploading signature file...")

        tor_sig_id = await relay_files(
            client,
            callback,
            tor_sig_url,
            tor_sig_original_name
        )

        await client.send_message(user_id, "Uploading the program...")

        tor_bin_id = await relay_files(
            client,
            callback,
            tor_bin_url,
            tor_bin_original_name
        )

        if tor_sig_id == -1 or tor_bin_id == -1:
            await client.send_message(user_id, "Oops, something went wrong. Please try again.")
            return
        else:
            logging.warning(f"New entry:")
            logging.warning(f"Binary name: {tor_bin_original_name}: ")
            logging.warning(f"Binary cache ID: {tor_bin_id}: ")
            logging.warning(f"Signature name: {tor_sig_original_name}:")
            logging.warning(f"Signature cache ID: {tor_sig_id}")

            curs.execute(
                f'''INSERT INTO tor_releases VALUES (
                    '{tor_bin_original_name}', '{tor_bin_id}', '{tor_sig_original_name}', '{tor_sig_id}'
                );'''
            )

            conn.commit()

    await client.answer_callback_query(
        callback.id
    )


def init():
    global conn, curs
    logging.debug("=== OnionSproutsBot ===")

    conn = sqlite3.connect('onionsproutsbot_ids.db')
    curs = conn.cursor()

    curs.execute(
        '''CREATE TABLE IF NOT EXISTS tor_releases (
            binary TEXT, binary_id INTEGER, sig TEXT, sig_id INTEGER,
            UNIQUE(binary, binary_id, sig, sig_id)
        );'''
    )

    conn.commit()
    OnionSproutsBot.run()
    conn.close()

init()
