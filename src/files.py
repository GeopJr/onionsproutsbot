#!/usr/bin/env python3
#
# This file is part of OnionSpoutsBot, a Tor Browser distribution Telegram bot.
#
# :authors: Panagiotis "Ivory" Vasilopoulos <git@n0toose.net>
#
# :copyright:   (c) 2020-2022, Panagiotis "Ivory" Vasilopoulos
#
# :license: This is Free Software. See LICENSE for license information.
#

import aiofiles
import asyncio
import logging
import requests
import tempfile

from pyrogram import Client

# TODO: Show a progress bar in a Telegram message instead of using stdout.
async def progress(current, total):
    logging.debug(f"{current / total * 100:.1f}%")

async def relay_files(
    client: Client,
    callback: str,
    url: str,
    original_name: str
) -> int:
    data = requests.get(url, allow_redirects=True, stream=True)
    send_success = False

    async with aiofiles.tempfile.NamedTemporaryFile('wb', delete=False) as file_object:
        await file_object.write(data.content)
        await file_object.seek(0)

    try:
        file_object = await client.send_document(
            callback.from_user.id,
            document = file_object.name,
            file_name = original_name,
            progress = progress
        )

        file_id = file_object["document"]["file_id"]
        send_success = True
    except Exception as e:
        logging.exception(e)
        await client.send_message(
            callback.from_user.id,
            f"Upload failed! Reason: `{e}`"
        )

    if send_success == True:
        return file_id
    else:
        return -1

